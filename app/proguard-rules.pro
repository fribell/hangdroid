# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn *.**

-printseeds seeds.txt
-printusage unused.txt
-printmapping mapping.txt
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-keepattributes *Annotation*, Signature, SourceFile, LineNumberTable, ElementList, Root

# Preserve all public classes, and their public and protected fields and
# methods.
-keep public class * {
    public protected *;
}

-keep class android.support.v4.app.** { *; }
-keep class android.support.v7.** { *; }
-keep class android.support.v13.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep interface android.support.v13.** { *; }
-keep interface android.support.v7.** { *; }
-keep class com.android.vending.billing.**

-keep public class org.simpleframework.** { *; }
-keep class org.simpleframework.xml.** { *; }
-keep class org.simpleframework.xml.core.** { *; }
-keep class org.simpleframework.xml.util.** { *; }

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}


-keepclassmembers class * {
    @org.simpleframework.xml.* *;
}

##---------------Begin: proguard configuration for Gson  ----------
# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.ribell.hangdroid.model.** { *; }
##---------------End: proguard configuration for Gson  ----------