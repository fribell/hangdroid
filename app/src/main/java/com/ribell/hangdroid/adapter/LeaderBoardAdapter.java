package com.ribell.hangdroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ribell.hangdroid.R;
import com.ribell.hangdroid.model.LeaderBoardObject;

import java.util.ArrayList;

/**
 * Created by ferranribell on 28/09/2014.
 */
public class LeaderBoardAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<LeaderBoardObject> values;

    public LeaderBoardAdapter(Context context, ArrayList<LeaderBoardObject> values){
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imgIcon;
        TextView txtTitle;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.leaderboard_list_item, null);

            imgIcon = convertView.findViewById(R.id.icon);
            txtTitle = convertView.findViewById(R.id.title);
            convertView.setTag(new ViewHolder(imgIcon, txtTitle));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            imgIcon = viewHolder.imgIcon;
            txtTitle = viewHolder.txtTitle;
        }
        imgIcon.setImageResource(values.get(position).getDrawable());
        txtTitle.setText(values.get(position).getTitle());

        return convertView;
    }

    private static class ViewHolder {
        public final ImageView imgIcon;
        public final TextView txtTitle;

        public ViewHolder(ImageView imgIcon, TextView txtTitle) {
            this.imgIcon = imgIcon;
            this.txtTitle = txtTitle;
        }
    }
}
