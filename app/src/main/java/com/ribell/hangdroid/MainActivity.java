package com.ribell.hangdroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.ribell.hangdroid.fragments.GameFragment;
import com.ribell.hangdroid.fragments.MainFragment;
import com.ribell.hangdroid.fragments.SettingsFragment;
import com.ribell.hangdroid.fragments.StatsFragment;
import com.ribell.hangdroid.helper.BillingHelperKt;
import com.ribell.hangdroid.helper.CastHelper;
import com.ribell.hangdroid.helper.FragmentHelper;
import com.ribell.hangdroid.helper.GoogleApiHelper;
import com.ribell.hangdroid.helper.PurchasesResponseListener;
import com.ribell.hangdroid.listener.CastListener;
import com.ribell.hangdroid.listener.SigninListener;
import com.ribell.hangdroid.model.Message;
import com.ribell.hangdroid.model.MessageKt;
import com.ribell.hangdroid.utils.PreferencesManagement;
import com.ribell.hangdroid.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import static com.ribell.hangdroid.fragments.SettingsFragment.SKU_ADS;


public class MainActivity extends AppCompatActivity implements PurchasesUpdatedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int MAIN_FRAGMENT = 0;
    private static final int HISTORY_FRAGMENT = 1;
    private static final int SETTINGS_FRAGMENT = 2;

    private int billingResponseCode;
    private BillingClient billingClient;
    private SkuDetails skuDetail;
    private boolean hasAds;
    private CastHelper castHelper;
    private GoogleApiHelper googleApiHelper;
    private BottomNavigationView bottomNavigationView;
    private CastListener castListener = new CastListener() {

        @Override
        public void onDisconnected() {
            if (bottomNavigationView.getSelectedItemId() == R.id.action_game) {
                showFragment(MAIN_FRAGMENT);
            }
        }

        @Override
        public void onConnected() {

        }
    };

    private SigninListener signinListener = new SigninListener() {
        @Override
        public void onSignedIn() {
            StatsFragment statsFragment = (StatsFragment) getSupportFragmentManager().findFragmentByTag(StatsFragment.TAG);
            if (statsFragment != null && statsFragment.isAdded()) {
                statsFragment.hideSignIn();
            }
        }

        @Override
        public void onSignOut() {
            StatsFragment statsFragment = (StatsFragment) getSupportFragmentManager().findFragmentByTag(StatsFragment.TAG);
            if (statsFragment != null && statsFragment.isAdded()) {
                statsFragment.showSignIn();
            }
        }
    };

    public SkuDetails getSkuDetail() {
        return skuDetail;
    }

    public BillingClient getBillingClient() {
        return billingClient;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setCustomActionBar();

        Utils.changeLanguage(this, PreferencesManagement.getLanguageCode(this));
        castHelper = new CastHelper(this, castListener);
        googleApiHelper = new GoogleApiHelper(this, signinListener);
        googleApiHelper.initSignIn();
        setBottomNavigationView();
        initBillingClient();
        showFragment(MAIN_FRAGMENT);
    }

    private void setCustomActionBar() {
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

    }

    private void setBottomNavigationView() {
        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        GameFragment gameFragment = (GameFragment) getSupportFragmentManager().findFragmentByTag(GameFragment.TAG);
                        if (gameFragment != null && gameFragment.isAdded() && gameFragment.isVisible()) {
                            onBackPressed();
                            return false;
                        } else {
                            switch (item.getItemId()) {
                                case R.id.action_game:
                                    showFragment(MAIN_FRAGMENT);
                                    break;

                                case R.id.action_stats:
                                    showFragment(HISTORY_FRAGMENT);
                                    break;

                                case R.id.action_settings:
                                    showFragment(SETTINGS_FRAGMENT);
                                    break;

                            }

                            return true;
                        }

                    }
                });
    }

    public GoogleApiHelper getGoogleApiHelper() {
        return googleApiHelper;
    }

    public CastHelper getCastHelper() {
        return castHelper;
    }

    private void showQuitPopup(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.quit_game)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String json = new Message(MessageKt.ACTION_SLEEP).toString();
                        sendMessage(json,
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status result) {
                                        if (!result.isSuccess()) {
                                            Log.e(TAG, "Sending message failed");
                                        }
                                        showFragment(position);
                                    }
                                });

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.setCancelable(false)
                .create().show();
    }

    private void showFragment(int position) {
        switch (position) {
            case MAIN_FRAGMENT:
                FragmentHelper.showMainFragment(getSupportFragmentManager());
                break;
            case HISTORY_FRAGMENT:
                FragmentHelper.showStatsFragment(getSupportFragmentManager());
                break;
            case SETTINGS_FRAGMENT:
                FragmentHelper.showSettingsFragment(getSupportFragmentManager());
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        castHelper.addMediaRouteItem(menu);
        return true;
    }

    @Override
    protected void onResume() {
        googleApiHelper.signInSilently();
        castHelper.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        castHelper.onPause();
    }


    @Override
    public void onBackPressed() {
        GameFragment gameFragment = (GameFragment) getSupportFragmentManager().findFragmentByTag(GameFragment.TAG);
        if (gameFragment == null || !gameFragment.isAdded()) {
            MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
            StatsFragment statsFragment = (StatsFragment) getSupportFragmentManager().findFragmentByTag(StatsFragment.TAG);

            if (mainFragment != null && mainFragment.isVisible()) {
                finish();
            } else if (statsFragment != null) {
                showFragment(MAIN_FRAGMENT);
            } else {
                super.onBackPressed();
            }

        } else {
            showQuitPopup(0);
        }
    }

    public void sendMessage(String json, ResultCallback<Status> resultCallback) {
        Log.d(TAG, "Message: " + json);
        castHelper.sendMessage(json, resultCallback);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, " ActivityResult responseCode:" + resultCode + " requestCode: " + requestCode);
        switch (requestCode) {
            case GoogleApiHelper.RC_SIGN_IN:
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                getGoogleApiHelper().handleSignInResult(task);
                break;

            case GoogleApiHelper.REQUEST_LEADERBOARD:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Google Play Services must be installed. " + resultCode, Toast.LENGTH_SHORT).show();
                }
                break;

            case SettingsFragment.RC_REQUEST:
                if (resultCode == RESULT_CANCELED) {
                    findViewById(R.id.progressbar).setVisibility(View.GONE);
                } else if (resultCode == RESULT_OK) {
                    findViewById(R.id.progressbar).setVisibility(View.GONE);
                    enableInAppPurchase(false);
                }

                super.onActivityResult(requestCode, resultCode, data);
                break;

            default:
                if (requestCode == billingResponseCode) {
                    Log.i(TAG, "billing flow finished");
                    enableInAppPurchase(false);
                }
                break;
        }
    }

    private void initBillingClient() {
        billingClient = BillingClient.newBuilder(this).setListener(this).build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    // The billing client is ready. You can query purchases here.
                    getPurchases();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    private void getPurchases() {
        BillingHelperKt.getPurchases(billingClient, new PurchasesResponseListener() {
            @Override
            public void onPurchasesResponse(@NotNull List<Purchase> purchasesList) {
                setAdsPurchase(purchasesList);
            }
        });
    }

    private void setAdsPurchase(List<Purchase> purchasesList) {
        if (purchasesList != null && purchasesList.size() > 0) {
            Log.i(TAG, "Purchased: " + purchasesList.get(0).getOriginalJson());
            hasAds = false;
            showAds(false);
            enableInAppPurchase(false);
        } else {
            Log.i(TAG, "Purchased false");
            hasAds = true;
            showAds(true);
            enableInAppPurchase(true);
            getSkusList();
        }
    }

    private void enableInAppPurchase(boolean enabled) {
        SettingsFragment settingsFragment = (SettingsFragment) getSupportFragmentManager().findFragmentByTag(SettingsFragment.TAG);
        if (settingsFragment != null) {
            settingsFragment.enableInAppPurchase(enabled);
        }
    }

    private void showAds(boolean show) {
        MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
        if (mainFragment != null) {
            mainFragment.showAds(show);
        }
    }

    private void getSkusList() {
        List skuList = new ArrayList<>();
        skuList.add(SKU_ADS);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                if (skuDetailsList != null && skuDetailsList.size() > 0) {
                    skuDetail = skuDetailsList.get(0);
                }
            }
        });
    }

    public void startBillingFlow() {
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSku(SKU_ADS)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        if (getSkuDetail() != null) {
            flowParams = BillingFlowParams.newBuilder()
                    .setSku(getSkuDetail().getSku())
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
        }
        billingResponseCode = getBillingClient().launchBillingFlow(this, flowParams);

    }

    public boolean isHasAds() {
        return hasAds;
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        switch (responseCode) {
            case BillingClient.BillingResponse.ITEM_ALREADY_OWNED:
                Snackbar.make(getCurrentFocus(),
                        R.string.error_item_already_owned, Snackbar.LENGTH_SHORT).show();
                hasAds = false;
                showAds(false);
                enableInAppPurchase(false);
                break;
            default:
                setAdsPurchase(purchases);
                break;
        }
    }
}
