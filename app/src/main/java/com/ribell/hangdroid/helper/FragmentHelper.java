package com.ribell.hangdroid.helper;

import com.ribell.hangdroid.R;
import com.ribell.hangdroid.fragments.GameFragment;
import com.ribell.hangdroid.fragments.MainFragment;
import com.ribell.hangdroid.fragments.SettingsFragment;
import com.ribell.hangdroid.fragments.StatsFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * Created by ferranribell on 30/09/2014.
 */
public class FragmentHelper {

    public static void showGameFragment(FragmentManager fragmentManager){
        GameFragment gameFragment = (GameFragment) fragmentManager.findFragmentByTag(GameFragment.TAG);
        if(gameFragment==null){
            gameFragment = new GameFragment();
        }
        showFragment(fragmentManager, gameFragment, GameFragment.TAG);
    }

    public static void showMainFragment(FragmentManager fragmentManager){
        MainFragment mainFragment = (MainFragment) fragmentManager.findFragmentByTag(MainFragment.TAG);
        if(mainFragment==null){
            mainFragment = MainFragment.newInstance();
        }
        showFragment(fragmentManager, mainFragment, MainFragment.TAG);
    }

    private static  void showFragment(FragmentManager fragmentManager, Fragment fragment, String tag){
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    public static void showStatsFragment(FragmentManager fragmentManager) {
        showFragment(fragmentManager, new StatsFragment(), StatsFragment.TAG);
    }

    public static void showSettingsFragment(FragmentManager fragmentManager) {
        SettingsFragment settingsFragment = (SettingsFragment) fragmentManager.findFragmentByTag(SettingsFragment.TAG);
        if(settingsFragment==null){
            settingsFragment = SettingsFragment.newInstance();
        }
        showFragment(fragmentManager, settingsFragment, SettingsFragment.TAG);
    }
}
