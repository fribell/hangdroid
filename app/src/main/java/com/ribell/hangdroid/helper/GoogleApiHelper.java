package com.ribell.hangdroid.helper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.ribell.hangdroid.AppController;
import com.ribell.hangdroid.R;
import com.ribell.hangdroid.listener.SigninListener;

import androidx.annotation.NonNull;

import static com.google.android.gms.common.api.CommonStatusCodes.SIGN_IN_REQUIRED;

/**
 * Created by ferranribell on 30/09/2014.
 */
public class GoogleApiHelper {
    public static final int RC_SIGN_IN = 0;
    public static final int REQUEST_LEADERBOARD = 1;
    private static String TAG = GoogleApiHelper.class.getSimpleName();
    private final GoogleSignInClient googleSignInClient;
    private final SigninListener listener;
    private Activity activity;
    private SignInButton btnSignIn;
    private ImageView imgProfilePic;
    private TextView txtName;
    private ImageView btnSignOut;

    public GoogleApiHelper(Activity activity, SigninListener listener) {
        this.activity = activity;
        this.listener = listener;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(activity, gso);
    }

    /**
     * Updating the UI, showing/hiding buttons and profile layout
     */
    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
            btnSignOut.setVisibility(View.VISIBLE);
            imgProfilePic.setVisibility(View.VISIBLE);
            txtName.setVisibility(View.VISIBLE);
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
            btnSignIn.setEnabled(true);
            btnSignOut.setVisibility(View.GONE);
            imgProfilePic.setVisibility(View.GONE);
            txtName.setVisibility(View.GONE);
        }
    }

    public void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(true);
            setProfileInformation(account);
            listener.onSignedIn();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            switch (e.getStatusCode()) {
                case SIGN_IN_REQUIRED:
                    Snackbar.make(activity.getCurrentFocus(),
                            R.string.error_sign_in_required, Snackbar.LENGTH_SHORT).show();
                    break;
            }
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Log.w(TAG, "signInResult:failed message=" + e.getMessage());
            updateUI(false);
        }
    }

    public void signInSilently() {

        getGoogleSignInClient().silentSignIn().addOnCompleteListener(activity,
                new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        updateUI(task.isSuccessful());
                        if (task.isSuccessful()) {
                            setProfileInformation(task.getResult());
                        }
                    }
                });
    }

    public void initSignIn() {
        btnSignIn = activity.findViewById(R.id.btn_sign_in);
        btnSignIn.setSize(SignInButton.SIZE_ICON_ONLY);
        btnSignOut = activity.findViewById(R.id.btn_sign_out);
        imgProfilePic = activity.findViewById(R.id.imgProfilePic);
        txtName = activity.findViewById(R.id.txtName);

        btnSignIn.setOnClickListener(new ClickListener());
        btnSignOut.setOnClickListener(new ClickListener());

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
        if (account != null) {
            setProfileInformation(account);
            updateUI(true);
        } else {
            updateUI(false);
        }

    }

    /**
     * Fetching user's information name, email, profile pic
     */
    private void setProfileInformation(GoogleSignInAccount account) {
        String personName = account.getDisplayName();
        Uri personPhotoUrl = account.getPhotoUrl();
        String email = account.getEmail();

        Log.d(TAG, "Name: " + personName + ", plusProfile:, email: " + email
                + ", Image: " + personPhotoUrl);

        txtName.setText(personName);

        if (personPhotoUrl != null) {
            ImageLoader imageLoader = AppController.getInstance().getImageLoader();
            imageLoader.get(personPhotoUrl.toString(), ImageLoader.getImageListener(
                    imgProfilePic, android.R.drawable.spinner_background, R.drawable.ic_launcher));
        }
        updateUI(true);
    }


    /**
     * Sign-out from google
     */
    private void signOut() {
        googleSignInClient.signOut()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(false);
                        revokeAccess();
                        listener.onSignOut();
                    }
                });
    }

    private void revokeAccess() {
        googleSignInClient.revokeAccess()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(false);
                    }
                });
    }

    public GoogleSignInClient getGoogleSignInClient() {
        return googleSignInClient;
    }

    public boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(activity) != null;
    }

    public void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_sign_in:
                    signIn();
                    break;
                case R.id.btn_sign_out:
                    signOut();
                    break;

            }
        }
    }
}
