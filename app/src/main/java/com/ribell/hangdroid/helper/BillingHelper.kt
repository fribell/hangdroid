package com.ribell.hangdroid.helper

import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

fun getPurchases(client: BillingClient, listener: PurchasesResponseListener) {
    val uiScope = CoroutineScope(Dispatchers.Main)
    uiScope.launch {
        withContext(Dispatchers.Default) {
            client.queryPurchases(BillingClient.SkuType.INAPP)
        }.let {
            listener.onPurchasesResponse(it.purchasesList)
        }
    }
}

interface PurchasesResponseListener {
    fun onPurchasesResponse(purchasesList: MutableList<Purchase>)
}
