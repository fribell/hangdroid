package com.ribell.hangdroid.helper;

import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastState;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.ribell.hangdroid.MainActivity;
import com.ribell.hangdroid.R;
import com.ribell.hangdroid.callback.HangDroidChannelCallback;
import com.ribell.hangdroid.cast.SessionManagerListenerImpl;
import com.ribell.hangdroid.listener.CastListener;

import java.io.IOException;

/**
 * Created by ferranribell on 30/09/2014.
 */
public class CastHelper {
    private final CastContext castContext;
    private final SessionManagerListenerImpl sessionManagerListener;
    private final CastListener castListener;
    public HangDroidChannelCallback hangDroidChannelCallback;
    private CastSession castSession;
    private String TAG = CastHelper.class.getSimpleName();
    private Activity activity;

    private SessionManager sessionManager;
    private MenuItem mediaRouteMenuItem;
    private CastStateListener castStateListener = new CastStateListener() {
        @Override
        public void onCastStateChanged(int newState) {
            switch (newState) {
                case CastState.CONNECTING:
                    //we should show a connecting dialog
                    break;
                case CastState.CONNECTED:
                    castListener.onConnected();
                    try {
                        getCurrentCastSession().setMessageReceivedCallbacks(hangDroidChannelCallback.getNamespace(), hangDroidChannelCallback);
                    } catch (Exception e) {
                        Log.d(TAG, "We couldn't get the session");
                    }
                    break;
                case CastState.NOT_CONNECTED:
                    castListener.onDisconnected();
                    break;
                default:
                    //showIntroductoryOverlay();
                    break;
            }
            Log.d(TAG, "New state: " + newState);
        }
    };

    public CastHelper(Activity activity, CastListener castListener) {
        this.activity = activity;
        this.castListener = castListener;
        castContext = CastContext.getSharedInstance(activity);
        castContext.addCastStateListener(castStateListener);
        hangDroidChannelCallback = new HangDroidChannelCallback(((MainActivity) activity).getSupportFragmentManager());

        sessionManager = CastContext.getSharedInstance(activity).getSessionManager();
        sessionManagerListener = new SessionManagerListenerImpl(activity);
    }

    private void showIntroductoryOverlay() {
        if (mediaRouteMenuItem != null) {
            IntroductoryOverlay overlay = new IntroductoryOverlay.Builder(activity, mediaRouteMenuItem)
                    .setTitleText(R.string.cast_intro_overlay_text)
                    .setOnOverlayDismissedListener(new IntroductoryOverlay.OnOverlayDismissedListener() {

                        @Override
                        public void onOverlayDismissed() {
                            // Maybe we can show something else
                        }
                    })
                    .setSingleTime()
                    .build();
            overlay.show();
        }
    }

    public boolean isApplicationStarted() {
        return CastContext.getSharedInstance(activity).getCastState() == CastState.CONNECTED;
    }

    public void addMediaRouteItem(Menu menu) {
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(activity,
                menu,
                R.id.media_route_menu_item);

    }

    public void sendMessage(String json, ResultCallback<Status> resultCallback) {
        if (hangDroidChannelCallback != null) {
            try {
                getCurrentCastSession().sendMessage(hangDroidChannelCallback.getNamespace(), json)
                        .setResultCallback(resultCallback);
            } catch (Exception e) {
                Log.e(TAG, "Exception while sending message", e);
            }
        }
    }

    private CastSession getCurrentCastSession() throws Exception {
        castSession = sessionManager.getCurrentCastSession();
        if (castSession == null) {
            throw new Exception("There is no session");
        }
        return castSession;
    }

    public void onResume() {

        try {
            getCurrentCastSession().setMessageReceivedCallbacks(hangDroidChannelCallback.getNamespace(), hangDroidChannelCallback);
        } catch (IOException e) {
            Log.e(TAG, "Exception while creating channel", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception while trying to get the session", e);
        }
        sessionManager.addSessionManagerListener(sessionManagerListener);

    }

    public void onPause() {
        sessionManager.removeSessionManagerListener(sessionManagerListener);
        castSession = null;
    }
}
