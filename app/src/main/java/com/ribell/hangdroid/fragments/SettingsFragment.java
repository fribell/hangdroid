package com.ribell.hangdroid.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ribell.hangdroid.MainActivity;
import com.ribell.hangdroid.R;
import com.ribell.hangdroid.utils.PreferencesManagement;
import com.ribell.hangdroid.utils.Utils;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

/**
 * Created by ferranribell on 13/07/15.
 */
public class SettingsFragment extends Fragment {
    public static final int RC_REQUEST = 10001;
    public static final String SKU_ADS = "advertisment";
    public static String TAG = SettingsFragment.class.getSimpleName();
    private View rootView;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    private void enableAdsPurchase() {
        rootView.findViewById(R.id.ads_button).setAlpha(1);
        rootView.findViewById(R.id.ads_button).setEnabled(true);
        rootView.findViewById(R.id.ads_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().startBillingFlow();
            }
        });
    }

    private void disableAdsPurchase() {
        rootView.findViewById(R.id.ads_button).setEnabled(false);
        rootView.findViewById(R.id.ads_button).setEnabled(false);
        rootView.findViewById(R.id.ads_button).setAlpha((float) 0.5);
        rootView.findViewById(R.id.ads_button).setOnClickListener(null);
        rootView.findViewById(R.id.icon_check).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.price_content).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        Utils.changeLanguage(getActivity(), PreferencesManagement.getLanguageCode(getActivity()));
        setSoundButton();
        setLanguageButton();
        if (getMainActivity().isHasAds()) {
            enableAdsPurchase();
        } else {
            disableAdsPurchase();
        }
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Utils.changeLanguage(getActivity(), PreferencesManagement.getLanguageCode(getActivity()));
    }

    private void setLanguageButton() {
        rootView.findViewById(R.id.language_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.pick_language)
                        .setItems(R.array.language_array, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Utils.changeLanguage(getActivity(), getActivity().getResources().getStringArray(R.array.language_local_code_array)[which]);
                                Intent refresh = new Intent(getActivity(), MainActivity.class);
                                getActivity().finish();
                                getActivity().startActivity(refresh);
                            }
                        });

                builder.create().show();
            }
        });
        ((TextView) rootView.findViewById(R.id.languagge_selected)).setText(Utils.getLanguageString(getActivity()));
    }

    private void setSoundButton() {
        if (PreferencesManagement.isSoundEnable(getActivity())) {
            enableSoundButton();
        } else {
            disableSoundButton();
        }
    }

    private void disableSoundButton() {
        ((TextView) rootView.findViewById(R.id.sound_description)).setText(getString(R.string.turn_sound_on));
        ((ImageView) rootView.findViewById(R.id.sound_icon)).setImageDrawable(getResources().getDrawable(R.drawable.speaker_off));
        rootView.findViewById(R.id.sound_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesManagement.setSoundSetting(getActivity(), true);
                enableSoundButton();
            }
        });
    }

    private void enableSoundButton() {
        ((TextView) rootView.findViewById(R.id.sound_description)).setText(getString(R.string.turn_sound_off));
        ((ImageView) rootView.findViewById(R.id.sound_icon)).setImageDrawable(getResources().getDrawable(R.drawable.speaker));
        rootView.findViewById(R.id.sound_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesManagement.setSoundSetting(getActivity(), false);
                disableSoundButton();
            }
        });
    }

    public void enableInAppPurchase(boolean enabled) {
        rootView.findViewById(R.id.ads_button).setEnabled(enabled);
        if (enabled) {
            enableAdsPurchase();
        } else {
            disableAdsPurchase();
        }
    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
