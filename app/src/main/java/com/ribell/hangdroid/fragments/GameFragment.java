package com.ribell.hangdroid.fragments;

/**
 * Created by ferranribell on 09/08/2014.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.ribell.hangdroid.LetterManagement;
import com.ribell.hangdroid.MainActivity;
import com.ribell.hangdroid.R;
import com.ribell.hangdroid.helper.FragmentHelper;
import com.ribell.hangdroid.listener.OnLetterClickListener;
import com.ribell.hangdroid.model.Message;
import com.ribell.hangdroid.model.MessageGame;
import com.ribell.hangdroid.model.MessageKt;
import com.ribell.hangdroid.model.MessageLetter;
import com.ribell.hangdroid.utils.PreferencesManagement;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class GameFragment extends Fragment
        implements OnLetterClickListener {
    public static final String TAG = "GAME_FRAGMENT";
    public static final String WIN_SCORE = "WIN_SCORE";
    public static final long ONE_SECOND = 1000L;
    private static final String ENGLISH = "ENG";
    private static final String SPANISH = "SPA";

    private View rootView;
    private int currentAttempts = 8;
    private Chronometer chronometer;
    private String word;
    private String language = ENGLISH;
    private GoogleSignInAccount googleSignInAccount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_game, container, false);
        chronometer = rootView.findViewById(R.id.chronometer);
        reloadGame();
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(getActivity());
        return rootView;
    }

    private void reloadGame() {
        language = PreferencesManagement.getGameLanguage(getActivity());
        String[] languageArray = getResources().getStringArray(R.array.language_code_array);
        int total = languageArray.length;
        for (int i = 0; i < total; i++) {
            if (language.equalsIgnoreCase(languageArray[i])) {
                ((TextView) rootView.findViewById(R.id.language)).setText(getString(R.string.language).concat(": ").concat(getResources().getStringArray(R.array.language_array)[i]));
                break;
            }
        }
        rootView.findViewById(R.id.loading).setVisibility(View.GONE);
        rootView.findViewById(R.id.number_attempts_container).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.lettersContainer).setVisibility(View.VISIBLE);
        LetterManagement.generateLetters(this, rootView);
        currentAttempts = 8;
        ((TextView) rootView.findViewById(R.id.num_attempts)).setText(getString(R.string.attempts) + " " + currentAttempts);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    @Override
    public void onClickLetter(String letter) {
        String json = new MessageLetter(MessageKt.ACTION_GAME, letter).toString();
        ((MainActivity) getActivity()).sendMessage(json, new ResultCallback<Status>() {
            @Override
            public void onResult(Status result) {
                if (!result.isSuccess()) {
                    Log.e(TAG, "Sending message failed");
                    Snackbar.make(rootView.findViewById(R.id.start_game), R.string.error_adding_letter, Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    @Override
    public void onWin(final String word) {
        this.word = word;
        chronometer.stop();
        if (getMainActivity().getGoogleApiHelper().isSignedIn()) {
            setGooglePlayGameLeadeBoards(word);
            setGooglePlayGameAchievements();
        }
        PreferencesManagement.addOneWinCounter(getActivity());
        showRestartGamePopup(R.string.win);
    }

    private void setGooglePlayGameAchievements() {
        switch (PreferencesManagement.getTotalWin(getMainActivity())) {
            case 50:
                Games.getAchievementsClient(getContext(), googleSignInAccount).unlock(getResources().getString(R.string.achivement_50));
                break;
            case 100:
                Games.getAchievementsClient(getContext(), googleSignInAccount).unlock(getResources().getString(R.string.achivement_100));
                break;
            case 500:
                Games.getAchievementsClient(getContext(), googleSignInAccount).unlock(getResources().getString(R.string.achivement_500));
                break;
            case 1000:
                Games.getAchievementsClient(getContext(), googleSignInAccount).unlock(getResources().getString(R.string.achivement_1000));
                break;
            case 5000:
                Games.getAchievementsClient(getContext(), googleSignInAccount).unlock(getResources().getString(R.string.achivement_5000));
                break;
        }
    }

    private void setGooglePlayGameLeadeBoards(final String word) {
        Games.getLeaderboardsClient(getContext(), googleSignInAccount)
                .loadCurrentPlayerLeaderboardScore(getString(R.string.leader_board_id), LeaderboardVariant.TIME_SPAN_DAILY, LeaderboardVariant.COLLECTION_PUBLIC)
                .addOnSuccessListener(new OnSuccessListener<AnnotatedData<LeaderboardScore>>() {
                    @Override
                    public void onSuccess(AnnotatedData<LeaderboardScore> leaderboardScoreAnnotatedData) {
                        long score = 0;
                        if (leaderboardScoreAnnotatedData.get() != null) {
                            score = leaderboardScoreAnnotatedData.get().getRawScore();
                        }
                        score += word.length();
                        Games.getLeaderboardsClient(getContext(), googleSignInAccount)
                                .submitScore(getResources().getString(R.string.leader_board_id), score, WIN_SCORE);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Error submiting score: " + e.getMessage());
            }
        });
        long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
        Games.getLeaderboardsClient(getContext(), googleSignInAccount)
                .submitScoreImmediate(getResources().getString(R.string.leader_board_time_id), elapsedMillis, String.valueOf(word.length()));
    }

    @Override
    public void onLose(String word) {
        this.word = word;
        chronometer.stop();
        PreferencesManagement.addOneLoseCounter(getActivity());
        showRestartGamePopup(R.string.lose);
    }

    private void showRestartGamePopup(int title) {
        PreferencesManagement.addOneWordCounter(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.end_game)
                .setTitle(title)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String json = new MessageGame(MessageKt.ACTION_NEW, language, PreferencesManagement.isSoundEnable(getActivity())).toString();
                        getMainActivity().sendMessage(json, new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status result) {
                                if (!result.isSuccess()) {
                                    Log.e(TAG, "Sending message failed");
                                    Snackbar.make(rootView, R.string.error_restarting_game, Snackbar.LENGTH_SHORT)
                                            .show();
                                } else {
                                    reloadGameAfterOneSecond();
                                }
                            }
                        });

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String json = new Message(MessageKt.ACTION_SLEEP).toString();
                        getMainActivity().sendMessage(json, new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status result) {
                                if (!result.isSuccess()) {
                                    Log.e(TAG, "Sending message failed");
                                }
                                FragmentHelper.showMainFragment(getActivity().getSupportFragmentManager());
                            }
                        });

                    }
                });

        builder.setCancelable(false)
                .create().show();
    }

    private void reloadGameAfterOneSecond() {
        rootView.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.number_attempts_container).setVisibility(View.GONE);
        rootView.findViewById(R.id.lettersContainer).setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                reloadGame();
            }
        }, ONE_SECOND);
    }

    @Override
    public void onAttemptChange(int attempts, String letter) {
        TextView attemptsTextView = rootView.findViewById(R.id.num_attempts);
        if (!isAdded()) {
            return;
        }
        if (currentAttempts != attempts) {
            attemptsTextView.setText(getString(R.string.attempts) + " " + attempts);
            Vibrator v = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);
            v.vibrate(300);
            currentAttempts = attempts;
        }

        LinearLayout ll = rootView.findViewWithTag(letter.toUpperCase());
        ll.setOnClickListener(null);
        ll.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
