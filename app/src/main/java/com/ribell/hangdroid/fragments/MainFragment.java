package com.ribell.hangdroid.fragments;

/**
 * Created by ferranribell on 09/08/2014.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.snackbar.Snackbar;
import com.ribell.hangdroid.MainActivity;
import com.ribell.hangdroid.R;
import com.ribell.hangdroid.helper.FragmentHelper;
import com.ribell.hangdroid.model.MessageGame;
import com.ribell.hangdroid.model.MessageKt;
import com.ribell.hangdroid.utils.PreferencesManagement;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {
    public static String TAG = MainFragment.class.getSimpleName();
    private View rootView;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        setStartGameButton();
        setDefaultLanguage();
        return rootView;
    }

    private void setDefaultLanguage() {
        String language = PreferencesManagement.getGameLanguage(getActivity());
        String[] languageArray = getResources().getStringArray(R.array.language_code_array);
        int total = languageArray.length;
        for (int i = 0; i < total; i++) {
            if (language.equalsIgnoreCase(languageArray[i])) {
                ((AppCompatSpinner) rootView.findViewById(R.id.language)).setSelection(i, true);
                break;
            }
        }
    }

    private void setStartGameButton() {
        rootView.findViewById(R.id.start_game).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!getMainActivity().getCastHelper().isApplicationStarted()) {
                    Toast.makeText(getMainActivity(), getString(R.string.loading_game), Toast.LENGTH_LONG).show();
                    return;
                }
                int languagePosition = ((AppCompatSpinner) rootView.findViewById(R.id.language)).getSelectedItemPosition();
                String lang = getResources().getStringArray(R.array.language_code_array)[languagePosition];
                PreferencesManagement.setGameLanguage(getActivity(), lang);
                v.setVisibility(View.GONE);
                rootView.findViewById(R.id.loading).setVisibility(View.VISIBLE);
                String json = new MessageGame(MessageKt.ACTION_NEW, lang, PreferencesManagement.isSoundEnable(getActivity())).toString();
                getMainActivity().sendMessage(json, new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status result) {
                        if (!result.isSuccess()) {
                            Log.e(TAG, "Sending message failed");
                            rootView.findViewById(R.id.start_game).setVisibility(View.VISIBLE);
                            Snackbar.make(rootView.findViewById(R.id.start_game), R.string.error_creating_new_game, Snackbar.LENGTH_SHORT)
                                    .show();
                        } else {
                            FragmentHelper.showGameFragment(getActivity().getSupportFragmentManager());
                        }
                        rootView.findViewById(R.id.loading).setVisibility(View.GONE);
                    }
                });

            }
        });
    }

    public void showAds(boolean show) {
        AdView adView = rootView.findViewById(R.id.adView);
        if (adView != null) {
            if (!show) {
                adView.removeAllViews();
            } else if (show) {
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);
            }
        }
    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
