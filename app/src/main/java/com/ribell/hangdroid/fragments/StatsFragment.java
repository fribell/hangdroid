package com.ribell.hangdroid.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.ribell.hangdroid.MainActivity;
import com.ribell.hangdroid.R;
import com.ribell.hangdroid.adapter.LeaderBoardAdapter;
import com.ribell.hangdroid.helper.GoogleApiHelper;
import com.ribell.hangdroid.model.LeaderBoardObject;
import com.ribell.hangdroid.utils.PreferencesManagement;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by ferranribell on 30/09/2014.
 */
public class StatsFragment extends Fragment {

    private static final int RC_ACHIEVEMENT_UI = 9003;
    private static final int RC_LEADERBOARD_UI = 9004;
    public static String TAG = StatsFragment.class.getSimpleName();
    private View rootView;
    private GoogleApiHelper googleApiHelper;
    private SignInButton signInButton;
    private ListView listView;
    private LinearLayout totalWords;
    private LinearLayout winCounter;
    private LinearLayout loseCounter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_stats, container, false);
        this.googleApiHelper = getMainActivity().getGoogleApiHelper();

        showLeaderBoard();
        setSignInButton();
        return rootView;
    }

    private void setSignInButton() {
        signInButton = rootView.findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMainActivity().getGoogleApiHelper().signIn();
            }
        });
        if (getMainActivity().getGoogleApiHelper().isSignedIn()) {
            hideSignIn();
        } else {
            showSignIn();
        }
    }

    private void showLeaderBoard() {
        totalWords = rootView.findViewById(R.id.total_words);
        winCounter = rootView.findViewById(R.id.win_counter);
        loseCounter = rootView.findViewById(R.id.lose_counter);

        totalWords.findViewById(R.id.image).setBackground(getResources().getDrawable(R.drawable.words_counter));
        ((TextView) totalWords.findViewById(R.id.counter)).setText(String.valueOf(PreferencesManagement.getTotalWord(getActivity())));

        winCounter.findViewById(R.id.image).setBackground(getResources().getDrawable(R.drawable.thumbup));
        ((TextView) winCounter.findViewById(R.id.counter)).setText(String.valueOf(PreferencesManagement.getTotalWin(getActivity())));

        loseCounter.findViewById(R.id.image).setBackground(getResources().getDrawable(R.drawable.thumbdown));
        ((TextView) loseCounter.findViewById(R.id.counter)).setText(String.valueOf(PreferencesManagement.getTotalLose(getActivity())));

        listView = rootView.findViewById(R.id.leaderboards_list);
        ArrayList<LeaderBoardObject> leaderBoardList = new ArrayList<LeaderBoardObject>(1);
        leaderBoardList.add(new LeaderBoardObject(R.drawable.crown, getString(R.string.show_ranking), getString(R.string.leader_board_id), LeaderBoardObject.TYPE_LEADER_BOARD));
        leaderBoardList.add(new LeaderBoardObject(R.drawable.crown, getString(R.string.show_time_ranking), getString(R.string.leader_board_time_id), LeaderBoardObject.TYPE_LEADER_BOARD));
        leaderBoardList.add(new LeaderBoardObject(R.drawable.crown, getString(R.string.displaying_achievements), null, LeaderBoardObject.TYPE_ACHIEVEMENTS));

        LeaderBoardAdapter leaderBoardAdapter = new LeaderBoardAdapter(getMainActivity(), leaderBoardList);

        listView.setAdapter(leaderBoardAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                if (googleApiHelper.isSignedIn()) {
                    LeaderBoardObject leaderBoardObject = (LeaderBoardObject) adapter.getItemAtPosition(position);
                    switch (leaderBoardObject.getType()) {
                        case LeaderBoardObject.TYPE_ACHIEVEMENTS:
                            Games.getAchievementsClient(getContext(), GoogleSignIn.getLastSignedInAccount(getContext()))
                                    .getAchievementsIntent()
                                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                                        @Override
                                        public void onSuccess(Intent intent) {
                                            startActivityForResult(intent, RC_ACHIEVEMENT_UI);
                                        }
                                    });
                            break;
                        case LeaderBoardObject.TYPE_LEADER_BOARD:
                            String leaderBoardId = leaderBoardObject.getId();
                            Games.getLeaderboardsClient(getContext(), GoogleSignIn.getLastSignedInAccount(getContext()))
                                    .getLeaderboardIntent(leaderBoardId)
                                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                                        @Override
                                        public void onSuccess(Intent intent) {
                                            startActivityForResult(intent, RC_LEADERBOARD_UI);
                                        }
                                    });
                            break;
                    }
                } else {
                    Snackbar.make(getView(), R.string.common_signin_button_text_long, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    public void hideSignIn() {
        signInButton.setVisibility(GONE);
        listView.setVisibility(VISIBLE);
        totalWords.setVisibility(VISIBLE);
        winCounter.setVisibility(VISIBLE);
        loseCounter.setVisibility(VISIBLE);
    }

    public void showSignIn() {
        listView.setVisibility(GONE);
        signInButton.setVisibility(View.VISIBLE);
        totalWords.setVisibility(GONE);
        winCounter.setVisibility(GONE);
        loseCounter.setVisibility(GONE);
    }
}
