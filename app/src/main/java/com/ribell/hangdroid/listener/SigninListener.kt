package com.ribell.hangdroid.listener

interface SigninListener {
    fun onSignedIn()
    fun onSignOut()
}