package com.ribell.hangdroid.listener

interface CastListener {
    fun onConnected()
    fun onDisconnected()
}