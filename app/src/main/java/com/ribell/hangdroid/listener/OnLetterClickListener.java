package com.ribell.hangdroid.listener;

/**
 * Created by Ferran Ribell on 26/07/14.
 */
public interface OnLetterClickListener {
    void onClickLetter(String letter);

    void onWin(String word);

    void onLose(String word);

    void onAttemptChange(int attempts, String letter);
}
