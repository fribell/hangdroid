package com.ribell.hangdroid.callback;

import android.util.Log;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ribell.hangdroid.fragments.GameFragment;
import com.ribell.hangdroid.helper.FragmentHelper;
import com.ribell.hangdroid.listener.OnLetterClickListener;

import androidx.fragment.app.FragmentManager;

/**
 * Custom message channel
 */
public class HangDroidChannelCallback implements Cast.MessageReceivedCallback {
    private static final String GAME_LOAD = "loaded";
    private static final String TAG = HangDroidChannelCallback.class.getSimpleName();
    private static String NAME_SPACE = "urn:x-cast:com.ribell.hangdroid";
    private FragmentManager fragmentManager;
    private OnLetterClickListener onLetterClickListener;

    public HangDroidChannelCallback(FragmentManager supportFragmentManager) {
        fragmentManager = supportFragmentManager;
    }

    private OnLetterClickListener getOnLetterClickListener() {
        if (onLetterClickListener == null) {
            onLetterClickListener = (OnLetterClickListener) fragmentManager.findFragmentByTag(GameFragment.TAG);
        }
        return onLetterClickListener;
    }

    /**
     * @return custom namespace
     */
    public String getNamespace() {
        return NAME_SPACE;
    }

    /*
     * Receive message from the receiver app
     */
    @Override
    public void onMessageReceived(CastDevice castDevice, String namespace,
                                  String message) {
        Log.d(TAG, "onMessageReceived: " + message);
        parserResponse(message);
    }

    private void parserResponse(String message) {
        Log.d(TAG, "Response: " + message);
        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(message, JsonElement.class).getAsJsonObject();
        if (jsonObject.has("win")) {
            String word = jsonObject.get("word").getAsString();
            if (jsonObject.get("win").getAsBoolean()) {
                getOnLetterClickListener().onWin(word);
            } else {
                getOnLetterClickListener().onLose(word);
            }

            return;
        } else if (jsonObject.has("attempts")) {
            getOnLetterClickListener().onAttemptChange(jsonObject.get("attempts").getAsInt(), jsonObject.get("letter").getAsString());
        } else if (jsonObject.has("game") && jsonObject.get("game").getAsString().equalsIgnoreCase(GAME_LOAD)) {
            FragmentHelper.showGameFragment(fragmentManager);
        }
    }
}

