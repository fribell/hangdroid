package com.ribell.hangdroid;

import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ribell.hangdroid.listener.OnLetterClickListener;

import androidx.fragment.app.Fragment;

import static android.view.View.inflate;

/**
 * Created by ferran ribell on 26/07/14.
 */
public class LetterManagement {
    public static void generateLetters(final Fragment fragment, View rootView){
        GridLayout gridLayout = rootView.findViewById(R.id.lettersContainer);
        gridLayout.removeAllViews();
        for(char alphabet = 'A'; alphabet <= 'Z';alphabet++) {
            LinearLayout ll = (LinearLayout) inflate(rootView.getContext(), R.layout.letter, null);
            //ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            ll.setMinimumWidth(rootView.getContext().getResources().getDimensionPixelSize(R.dimen.button_letter_size));
            ll.setMinimumHeight(rootView.getContext().getResources().getDimensionPixelSize(R.dimen.button_letter_size));
            ((TextView) ll.findViewById(R.id.letter)).setText(String.valueOf(alphabet));
            ll.setTag(String.valueOf(alphabet));
            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((OnLetterClickListener) fragment).onClickLetter(v.getTag().toString());
                }
            });
            gridLayout.addView(ll);
        }
        TextView attempts = rootView.findViewById(R.id.num_attempts);
        attempts.setText(attempts.getText()+" 8");
    }

}
