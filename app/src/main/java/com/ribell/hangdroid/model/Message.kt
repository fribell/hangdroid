package com.ribell.hangdroid.model


const val ACTION_NEW: String = "new"
const val ACTION_SLEEP: String = "sleep"
const val ACTION_GAME: String = "game"

data class Message(var action: String) {
    override fun toString(): String = "{\"action\": \"$action\"}"
}

data class MessageLetter(var action: String, var letter: String) {
    override fun toString(): String = "{\"action\": \"$action\", \"letter\": \"$letter\"}"
}

data class MessageGame(var action: String, var lang: String, var sound: Boolean) {
    override fun toString(): String = "{\"action\": \"$action\", \"lang\": \"$lang\", \"sound\": $sound}"
}
