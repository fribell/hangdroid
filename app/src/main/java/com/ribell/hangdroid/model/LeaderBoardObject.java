package com.ribell.hangdroid.model;

/**
 * Created by ferranribell on 07/10/2014.
 */
public class LeaderBoardObject {
    public static final int TYPE_LEADER_BOARD = 0;
    public static final int TYPE_ACHIEVEMENTS = 1;
    private int drawable;
    private String title;
    private String id;
    private int type;

    public LeaderBoardObject(int drawable, String title, String id, int type) {
        this.drawable = drawable;
        this.title = title;
        this.id = id;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDrawable() {
        return drawable;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}
