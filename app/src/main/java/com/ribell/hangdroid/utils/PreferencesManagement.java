package com.ribell.hangdroid.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Locale;

/**
 * Created by ferranribell on 07/10/2014.
 */
public class PreferencesManagement {
    private static final String WORD_COUNTER = "WORD_COUNTER";
    private static final String WIN_COUNTER = "WIN_COUNTER";
    private static final String LOSE_COUNTER = "LOSE_COUNTER";
    private static final String GAME_LANGUAGE = "GAME_LANGUAGE";
    private static final String SOUND_SETTING = "SOUND_SETTING";
    private static final String APP_LANGUAGE_CODE = "APP_LANGUAGE_CODE";
    private static String PREFS_KEY = "HANGDROID_PREFERENCES";

    public static void addOneWordCounter(Context context){
        int currentValue = getPreferences(context).getInt(WORD_COUNTER, 0);
        getPreferences(context).edit().putInt(WORD_COUNTER, currentValue + 1).apply();
    }

    public static void addOneWinCounter(Context context){
        int currentValue = getPreferences(context).getInt(WIN_COUNTER, 0);
        getPreferences(context).edit().putInt(WIN_COUNTER, currentValue + 1).apply();
    }

    public static void addOneLoseCounter(Context context){
        int currentValue = getPreferences(context).getInt(LOSE_COUNTER, 0);
        getPreferences(context).edit().putInt(LOSE_COUNTER, currentValue + 1).apply();
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREFS_KEY, 0);
    }

    public static int getTotalWord(Context context) {
        return getPreferences(context).getInt(WORD_COUNTER, 0);
    }

    public static int getTotalWin(Context context) {
        return getPreferences(context).getInt(WIN_COUNTER, 0);
    }

    public static int getTotalLose(Context context) {
        return getPreferences(context).getInt(LOSE_COUNTER, 0);
    }

    public static String getGameLanguage(Context context) {
        return getPreferences(context).getString(GAME_LANGUAGE, "ENG");
    }

    public static void setGameLanguage(Context context, String lang){
        getPreferences(context).edit().putString(GAME_LANGUAGE, lang).apply();
    }

    public static void setSoundSetting(Context context, Boolean bol){
        getPreferences(context).edit().putBoolean(SOUND_SETTING, bol).apply();
    }

    public static boolean isSoundEnable(Context context) {        
        return getPreferences(context).getBoolean(SOUND_SETTING, true);
    }

    public static void setLanguageCode(Context context, String lang) {
        getPreferences(context).edit().putString(APP_LANGUAGE_CODE, lang).apply();
    }

    public static String getLanguageCode(Context context) {
        return getPreferences(context).getString(APP_LANGUAGE_CODE, Locale.getDefault().getLanguage());
    }
}
