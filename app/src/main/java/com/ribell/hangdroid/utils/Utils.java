package com.ribell.hangdroid.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;

import com.ribell.hangdroid.R;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Created by ferranribell on 13/07/15.
 */
public class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    public static String toMD5(String message) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(message.getBytes());
            message = new BigInteger(1, messageDigest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "No such algorithm found");
        }
        return message;
    }

    public static void changeLanguage(Context context, String lang) {

        PreferencesManagement.setLanguageCode(context, lang);

        Locale myLocale = new Locale(lang);
        if (lang.indexOf("-") > 0) {
            myLocale = new Locale(lang.split("-")[0], lang.split("-")[1]);
        }
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public static String getLanguageString(Context context) {
        String[] languageCodeArray = context.getResources().getStringArray(R.array.language_local_code_array);
        int total = languageCodeArray.length;
        String currentLocal = PreferencesManagement.getLanguageCode(context);
        for (int i = 0; i < total; i++) {
            if (currentLocal.equalsIgnoreCase(languageCodeArray[i])) {
                return context.getResources().getStringArray(R.array.language_array)[i];
            }
        }
        return context.getResources().getStringArray(R.array.language_array)[0];
    }
}
