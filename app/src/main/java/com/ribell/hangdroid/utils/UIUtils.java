package com.ribell.hangdroid.utils;

import android.content.Context;

import com.ribell.hangdroid.R;

import androidx.appcompat.app.AlertDialog;

/**
 * Created by ferranribell on 13/07/15.
 */
public class UIUtils {

    public static void alert(Context context, String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(context);
        bld.setMessage(message);
        bld.setNeutralButton(context.getString(R.string.ok), null);
        bld.create().show();
    }
}
